# POC réalisé avec Spring Boot et documentation avec Swagger

### Lancement de l'API :

   * Créez l'archive .war du projet
   * Déployez-le sur un serveur Tomcat
    
### Utilisation de Swagger :

   * Rendez-vous sur l'URL [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html) pour avoir l'interface de Swagger
   * Requêtez l'application

### Documentation du service REST : 

| Méthode | URL                                       |  BODY  | Description                                                                                                                   |
|---------|-------------------------------------------|--------|-------------------------------------------------------------------------------------------------------------------------------|
| GET     | /Meals                                    |        | Retourne l'ensemble des plats                                                                                                 |
| POST    | /Meal                                     |  Meal  | Ajoute un plat                                                                                                                |
| GET     | /Meal/{id}                                |        | Retourne le plat identifié par {id}                                                                                           |
| PUT     | /Meal/{id}                                |  Meal  | Modifie le plat identifié par {id}                                                                                            |
| DELETE  | /Meal/{id}                                |        | Supprime le plat identifié par {id }                                                                                          |