package com.pocm2gil.springbootpoc.dao;
import com.pocm2gil.springbootpoc.model.Meal;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.ArrayList;

@Repository
public class MealDaoImpl implements MealDao {

    public static List<Meal>meals=new ArrayList<>();
    static {
        meals.add(new Meal(1, "Nuggets", "Des poussins frits !"));
        meals.add(new Meal(2, "Hamburger", "Pour devenir gros !"));
        meals.add(new Meal(3, "Frites", "Une fois !"));
    }
    
    @Override
    public List<Meal> findAll() {
        return meals;
    }

    @Override
    public Meal findById(int id) {
        for (Meal meal : meals) {
            if (meal.getId() == id){
                return meal;
            }
        }
        return null;
    }

    @Override
    public Meal save(Meal meal) {
        meals.add(meal);
        return meal;
    }

    @Override
    public Meal update(int id, Meal meal) {
        Meal oldMeal = findById(id);
        meals.remove(oldMeal);
        Meal newMeal = meal;
        newMeal.setId(id);
        meals.add(newMeal);
        return newMeal;
    }

    @Override
    public List<Meal> delete(int id) {
        Meal meal = findById(id);
        meals.remove(meal);
        return meals;
    }
}