package com.pocm2gil.springbootpoc.dao;
import com.pocm2gil.springbootpoc.model.Meal;
import java.util.List;

public interface MealDao {
    public List<Meal> findAll();
    public Meal findById(int id);
    public Meal save(Meal meal);
    public Meal update(int id, Meal meal);
    public List<Meal> delete(int id);
}
