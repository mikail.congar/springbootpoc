package com.pocm2gil.springbootpoc.web.controller;
import com.pocm2gil.springbootpoc.dao.MealDao;
import com.pocm2gil.springbootpoc.model.Meal;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description="Une petite API sur des plats.")
@RestController
public class MealController {

    @Autowired
    private MealDao mealDao;

    @ApiOperation(value = "Permet d'obtenir la liste de tous les plats.")
    @GetMapping(value = "/Meals")
    public List<Meal> listMeals() {
        return mealDao.findAll();
    }

    @ApiOperation(value = "Permet d'obtenir des informations sur le plat de l'id selectionné.")
    @GetMapping(value = "/Meal/{id}")
    public Meal listOneMeal(@PathVariable int id) {
        return mealDao.findById(id);
    }

    @ApiOperation(value = "Permet d'ajouter un plat dans la liste des plats.")
    @PostMapping(value = "/Meal")
    public void addMeal(@RequestBody Meal meal) {
        mealDao.save(meal);
    }

    @ApiOperation(value = "Permet de modifier le plat de l'id selectionné.")
    @PutMapping(value = "/Meal/{id}")
    public void modifyMeal(@PathVariable int id, @RequestBody Meal meal) {
        mealDao.update(id, meal);
    }

    @ApiOperation(value = "Permet de supprimer le plat de l'id selectionné.")
    @DeleteMapping(value = "/Meal/{id}")
    public void deleteMeal(@PathVariable int id) {
        mealDao.delete(id);
    }
}