package com.pocm2gil.springbootpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class SpringbootpocApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootpocApplication.class, args);
	}

}














