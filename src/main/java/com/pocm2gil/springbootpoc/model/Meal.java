package com.pocm2gil.springbootpoc.model;

public class Meal {

    // ATTRIBUTS
    private int id;
    private String nom;
    private String description;

    // CONSTRUCTEURS
    public Meal() {
    }

    public Meal(int id, String nom, String description) {
        this.id = id;
        this.nom = nom;
        this.description = description;
    }

    // METHODES
    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getDescription() {
        return description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Meal{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}